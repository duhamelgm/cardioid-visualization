let points = 200;
let factor = 1;
let color = true;
let factorStep = 0.005;

function setup() {
  createCanvas(600, 600);
}

function draw() {
  background(0);
  translate(width / 2, height / 2);
  let change = (2 * Math.PI) / points;
  let radius = width / 2 - 20;

  noFill();
  stroke(255);
  circle(0, 0, radius);

  let angle = Math.PI / 2;

  factor += factorStep;

  for (let i = 0; i < points; i++) {
    const x = radius * Math.cos(angle);
    const y = radius * Math.sin(angle);
    //circle(x, y, 5);
    angle += change;
  }

  angle = Math.PI / 2;
  let colorFactor = 10;

  for (let i = 0; i < points; i++) {
    const x1 = radius * Math.cos(angle);
    const y1 = radius * Math.sin(angle);
    let offsetPos = (i * factor) % points;
    const x2 = radius * Math.cos(change * offsetPos);
    const y2 = radius * Math.sin(change * offsetPos);

    let r = Math.sin(colorFactor * i + 0) * 100 + 128;
    let g = Math.sin(colorFactor * i + 2) * 100 + 128;
    let b = Math.sin(colorFactor * i + 4) * 100 + 128;

    if (color) {
      stroke(r, g, b);
    } else {
      stroke(255);
    }

    line(x1, y1, x2, y2);
    angle += change;
  }
}

document.querySelector("#points").addEventListener("input", e => {
  points = e.target.value;
});

document.querySelector("#factor").addEventListener("input", e => {
  factor = parseInt(e.target.value);
});

document.querySelector("#color").addEventListener("change", e => {
  color = e.target.checked;
});

document.querySelector("#play").addEventListener("click", e => {
  factorStep = 0.005;
  factor = 0;
});
document.querySelector("#stop").addEventListener("click", e => {
  factorStep = 0;
  factor = 1;
  document.querySelector("#factor").value = 1;
});

document.querySelector("#config-button").addEventListener("click", e => {
  document.querySelector("#configs").style.display = "flex";
});
document.querySelector(".fa-times").addEventListener("click", e => {
  document.querySelector("#configs").style.display = "none";
});
